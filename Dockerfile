# Specify the image and release tag from which we're working
FROM atlasamglab/bootcamp-2020:21.2.125

# Put the current repo (the one in which this Dockerfile resides) in the /Bootcamp directory
# Note that this directory is created on the fly and does not need to reside in the repo already
ADD . /Bootcamp

# Go into the /Bootcamp/build directory and make /Bootcamp/build the default working directory (again, it will create the directory if it doesn't already exist)
WORKDIR /Bootcamp/build

# Make sure we're starting from a fresh build dir
RUN sudo rm -rf /Bootcamp/build/*

# Create a run directory
RUN sudo mkdir -p /Bootcamp/run

# Source the ATLAS analysis environment
# Make sure the directory containing your analysis code (and the code inside it) is owned by atlas user
# Build your source code using cmake
RUN source ~/release_setup.sh && \
    sudo chown -R atlas /Bootcamp && \
    cmake ../source && \
    make

